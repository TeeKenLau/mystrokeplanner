package com.example.dknig.mystroke;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

import com.example.dknig.mystroke.R;

public class pop extends Activity {

    Button close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Create pop-up selection dialog for changing the speed of speech
        super.onCreate(savedInstanceState);
        close = (Button) findViewById(R.id.popup_close);
        setContentView(R.layout.pop_speecrate);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.6),(int)(height*.6));
    }


    // Change the speed of speech to the slow
    public void rateClick(View v) {
        EventTab.normalSpeechRate= 0.5;
    }
    // Change the speed of speech to the slow
    public void rateClick2(View v) {
        EventTab.normalSpeechRate = 1.0;
    }
    // Change the speed of speech to the fast
    public void rateClick3(View v) {
        EventTab.normalSpeechRate = 1.5;
    }
    // Close up the dialog
    public void close(View v) {
        this.finish();
    }

}
